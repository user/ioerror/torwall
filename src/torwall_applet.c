/*
 * Copyright (c) 2008 Moxie Marlinspike
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>

#include <panel-applet.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkbox.h>
#include <gtk/gtkimage.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#include "TORWallPreferences.h"
#include "TORWallAbout.h"

#define ACTIVE_IMAGE "/torwallActive.png"
#define INACTIVE_IMAGE "/torwallInactive.png"

static int torWallIsActive = 0;
static TORWallPreferences *preferences;
static TORWallAbout *about;

static gboolean
toggle_active (GtkWidget      *event_box, 
	       GdkEventButton *event,
	       gpointer        data)
{
  GtkWidget *image = (GtkWidget *)data;
  int enabled_successfully;
  int disabled_successfully;

  if (event->button != 1) return FALSE;

  if (!torWallIsActive) {
    // Add gtksu and call loadIpTables
    enabled_successfully = printf("This is a placeholder.\n"); // replace with priv hook;
    if (enabled_successfully) {
      gtk_image_set_from_file(GTK_IMAGE(image), GNOMEICONDIR ACTIVE_IMAGE);
      printf("torwall activated\n");
      torWallIsActive = 1;
    }
  } else {
    disabled_successfully = printf("This is a placeholder.\n"); // replace with priv hook;
    if (disabled_successfully) {
      printf("torwall is disabled\n");
      gtk_image_set_from_file(GTK_IMAGE(image), GNOMEICONDIR INACTIVE_IMAGE);
      // Add gtksu and call unloadIpTables
      torWallIsActive = 0;
    }
  }

  return TRUE;
}

static void display_preferences (BonoboUIComponent *uic, 
				 PanelApplet *applet, 
				 const gchar *verbname) 
{  
  torwall_preferences_display(preferences);
}


static void 
display_about_dialog (BonoboUIComponent *uic, 
		      PanelApplet *applet, 
		      const gchar *verbname) 
{
  torwall_about_display(about);
}

static const char Context_menu_xml [] =
"<popup name=\"button3\">\n"
"   <menuitem name=\"Properties Item\" "
"             verb=\"TorWallProperties\" "
"           _label=\"_Preferences...\"\n"
"          pixtype=\"stock\" "
"          pixname=\"gtk-properties\"/>\n"

"   <menuitem name=\"About Item\" "
"             verb=\"TorWallAbout\" "
"           _label=\"_About...\"\n"
"          pixtype=\"stock\" "
"          pixname=\"gnome-stock-about\"/>\n"
"</popup>\n";

static const BonoboUIVerb example_menu_verbs [] = {
  BONOBO_UI_VERB ("TorWallProperties", display_preferences),
  BONOBO_UI_VERB ("TorWallAbout", display_about_dialog),
  BONOBO_UI_VERB_END
};


static gboolean
torwall_applet_fill (PanelApplet *applet, const gchar *iid, gpointer data)
{
  GtkWidget *eventBox, *image;

  if (strcmp (iid, "OAFIID:TorWallApplet") != 0) return FALSE;

  about       = torwall_about_new();
  preferences = torwall_preferences_new(applet);
  image       = gtk_image_new_from_file(GNOMEICONDIR INACTIVE_IMAGE);
  eventBox    = gtk_event_box_new();

  gtk_container_add(GTK_CONTAINER (eventBox), image);

  panel_applet_setup_menu (PANEL_APPLET (applet), Context_menu_xml,
			   example_menu_verbs, applet);	
	
  g_signal_connect (G_OBJECT (eventBox), "button_press_event",
		    G_CALLBACK (toggle_active), image);

  gtk_container_add (GTK_CONTAINER (applet), eventBox);
  gtk_widget_show_all (GTK_WIDGET (applet));

  return TRUE;
}

PANEL_APPLET_BONOBO_FACTORY ("OAFIID:TorWallApplet_Factory",
                             PANEL_TYPE_APPLET,
                             "The Tor Firewall Applet",
                             "0",
                             torwall_applet_fill,
                             NULL);
