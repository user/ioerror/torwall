/*
 * Copyright (c) 2008 Moxie Marlinspike
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>

#include <panel-applet.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkbox.h>
#include <gtk/gtkimage.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <stdlib.h>

#include "TORWallPreferences.h"

#define GLADE_FILE PACKAGE_GLADE_DIR "/torwall.glade"

static gboolean
torwall_preferences_cancel (GtkWidget      *button, 
                gpointer        data)
{
  GladeXML *xml     = ((TORWallPreferences*)data)->xml;
  GtkWidget *widget = glade_xml_get_widget(xml, "preferencesDialog");

  gtk_widget_hide(GTK_WIDGET(widget));
}

static gboolean
torwall_preferences_ok (GtkWidget      *button, 
            gpointer        data)
{

  GtkWidget *widget;
  gboolean permitLocal, firewallOnly;
  gchar *uid;
  PanelApplet *applet;
  GladeXML *xml;
  TORWallPreferences *preferences;

  preferences  = (TORWallPreferences*)data;
  applet       = preferences->applet;
  xml          = preferences->xml;
  widget       = glade_xml_get_widget(xml, "firewallOption");
  firewallOnly = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget));
  widget       = glade_xml_get_widget(xml, "permitLocalTraffic");
  permitLocal  = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget));
  widget       = glade_xml_get_widget(xml, "torUid");
  uid          = gtk_entry_get_text(GTK_ENTRY(widget));

  panel_applet_gconf_set_string(applet, "torid", uid, NULL);
  panel_applet_gconf_set_bool(applet, "permit_local", permitLocal, NULL);
  panel_applet_gconf_set_bool(applet, "firewall_only", firewallOnly, NULL);
  panel_applet_gconf_set_bool(applet, "init", TRUE, NULL);

  widget = glade_xml_get_widget(xml, "preferencesDialog");
  gtk_widget_hide(GTK_WIDGET(widget));
}


static GladeXML* torwall_preferences_init(TORWallPreferences* preferences) {
  GtkWidget *widget;
  GladeXML *xml;

  xml    = glade_xml_new (GLADE_FILE, NULL, NULL);
  widget = glade_xml_get_widget (xml, "okButton");
  g_signal_connect (G_OBJECT (widget), "clicked",
                    G_CALLBACK (torwall_preferences_ok),
                    preferences);

  widget = glade_xml_get_widget (xml, "cancelButton");
  g_signal_connect (G_OBJECT (widget), "clicked",
                    G_CALLBACK (torwall_preferences_cancel), 
            preferences);

  return xml;
}

void torwall_preferences_display(TORWallPreferences *preferences) {
  GtkWidget *widget;

  PanelApplet *applet   = preferences->applet;
  GladeXML *xml         = preferences->xml;

  gchar *uid            = panel_applet_gconf_get_string(applet, "torid", NULL);
  gboolean permitLocal  = panel_applet_gconf_get_bool(applet, "permit_local", NULL);
  gboolean firewallOnly = panel_applet_gconf_get_bool(applet, "firewall_only", NULL);
  gboolean init         = panel_applet_gconf_get_bool(applet, "init", NULL);

  widget = glade_xml_get_widget (xml, "firewallOption");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget), firewallOnly || !init);
  widget = glade_xml_get_widget (xml, "proxyOption");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget), !firewallOnly && init);
  widget = glade_xml_get_widget (xml, "permitLocalTraffic");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget), permitLocal);

  widget = glade_xml_get_widget (xml, "torUid");
  gtk_entry_set_text(GTK_ENTRY(widget), (uid == NULL) ? "" : uid);

  widget = glade_xml_get_widget (xml, "preferencesDialog");

  gtk_widget_show_all(GTK_WIDGET(widget));
}

TORWallPreferences* torwall_preferences_new(PanelApplet *applet) {
  TORWallPreferences *preferences = (TORWallPreferences*)malloc(sizeof(TORWallPreferences));
  preferences->applet             = applet;
  preferences->xml                = torwall_preferences_init(preferences);

  return preferences;
}
